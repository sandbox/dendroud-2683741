Email Registration Password added to Email Registration module password-token
in account notification emails.

INSTALLATION
============

Required step:

1. Enable Email Registration module as you normally would.

2. Enable current module as you normally would.


